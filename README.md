# CA683-Group42

This repo contains code, documentation and analysis for CA683's Main Assignment. Th question answered is as follows: 'Can machine learning algorithms be used to predict an individual's nationality based on their responses to a Big Five Personality Traits survey'. This analysis followed the CRISP-DM methology.

# Overview of Repo
## Paper
The paper relating to this analysis can be accessed in the 'Paper' folder.

## Presentation
The video presentation (.mp4) can be accessed from the presentation folder, including accompanying slides

## Data
All data used in this analysis is found in this folder. Data was sourced from [Kaggle](https://www.kaggle.com/tunguz/big-five-personality-test/data) and orginated from the [Open-Source Psychometrics Project](https://openpsychometrics.org/). The raw data and transformed data is split into two files. 

## Code
All code was written in Python using the Google Colab environment. A brief description of each file is as follows:

| Colab File        | Description           | 
| :-- |:-------------| 
| 2. Data Understanding      | All code used in the Data Understanding phase of the analysis, including Missing/Invalid data points and Target Variable analysis | 
| 3. Data Preparation      | All code used in the Data Preparation phase including removing missing invalid rows and pca analysis      | 
| 4. MLP | Code used to implement various Multilayer Perceptron (MLP) deep learning models for the Modeling Phase      | 
| 4. Naive Bayes | Code used to implement Gaussian Naive Bayes for the Modeling Phase, including an experiment to analyse the difference in varying the number of Principal Components used      | 
| Decision Tree | Code used to implement single classification decision trees, random forests and EasyEnsemble models|
| MN Log Reg | Code used to implement various logistic regression models for the Modeling Phase      | 
| XGBoost | Code used to implement XGBoost for the Modeling Phase      | 

# Help/Contact
Any questions/queries contact any of Group 42 members